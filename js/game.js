/**
 * Part of a creature!
 */

var cell = 
{
    localPos: new vector(0, 0),
    parentCreature: null,
    type: -1,
    cellRef: -1,
    currentSensorSignal: 0, //only used for sensors, else forever zero
    lastSum: 0, //used for debug purposes
    energy: 1,
    maxEnergy: 1,
    debugShouldDraw: false,

    start: function()
    {
        this.transform.scale.x = 0;
        this.transform.scale.y = 0;

        var scaleUpTween = new TWEEN.Tween(this.transform.scale);

        scaleUpTween.to({x: [1.5, 1], y: [1.5, 1]}, Math.random() * 3200);

        scaleUpTween.easing(TWEEN.Easing.Back.Out);

        scaleUpTween.start();
    },

    activate: function(strength, deltaTime)
    {
        this.lastSum = strength;

        switch(this.type)
        {
            case TYPES.LOCOMOTOR:
                var actualStrength = Math.max(0, strength)
                if(actualStrength > 0)
                {
                    var power = this.parentCreature.consumeEnergy(actualStrength * deltaTime)
                    var rotated = this.localPos.rotate(this.parentCreature.transform.rotation);

                    this.parentCreature.transform.position = this.parentCreature.transform.position.subtract(rotated.stretch(deltaTime * power));
                }
                break;
            case TYPES.ROTATOR:
                var power = this.parentCreature.consumeEnergy(Math.abs(strength) * deltaTime)
                this.parentCreature.transform.rotation += (strength) * deltaTime * 100 * power;
                break;
        }
    },

    onCollision: function(other)
    {
        if (this.type == TYPES.MOUTH && !this.destroyed) 
        {
            if(other.parentCreature == this.parentCreature)
                return;

            this.parentCreature.gainEnergy(this.maxEnergy / 3);
            other.destroy();
        }
    },

    draw: function()
    {
        this.defaultDraw();

        if(this.debugShouldDraw)
        {
            GameWindow.canvasContext.fillStyle = 'black';
            GameWindow.canvasContext.fillText(this.lastSum, this.transform.position.x, this.transform.position.y);
        }
    },

    update: function()
    {
        var rotated = this.localPos.rotate(this.parentCreature.transform.rotation);
        this.transform.position = rotated.stretch(this.transform.scale.x).add(this.parentCreature.transform.position);

        if(this.type == TYPES.EYE)
            this.transform.rotation = rotated.toAngle();
        
        if(this.type == TYPES.LOCOMOTOR)
            this.transform.rotation = rotated.toAngle() - 90;

        if(this.type == TYPES.STOMACH)
            this.currentSensorSignal = (this.energy / this.maxEnergy);
    },

    setType: function(typeID)
    {
        this.renderer.subImage = typeID;
        this.type = typeID;
    }
} 
 
/**
 * It's a thing!
 */

var creature =
{
    dna: {},
    cellInstances: [],

    start: function()
    {

    },

    initCells()
    {
        for(var i = 0; i < this.dna.length; i++)
        {
            var newCell = GameObject.create(cell, this.transform.position.x + (this.dna[i].pos.x * 32), this.transform.position.y + (this.dna[i].pos.y * 32), s_cell, 400);

            newCell.setType(this.dna[i].type);
            newCell.parentCreature = this;
            newCell.localPos = new vector(this.dna[i].pos.x * 32, this.dna[i].pos.y * 32)

            newCell.cellRef = i;

            this.cellInstances.push(newCell);
        }
    },

    update: function(deltaTime)
    {
        for(var i = this.cellInstances.length -1; i >= 0; i--) //reverse loop for deletion of references to destroyed cells
        {
            if(this.cellInstances[i].destroyed)
                this.cellInstances.splice(i, 1);
        }

        for(var i = 0; i < this.cellInstances.length; i++) //loop through instances instead of dna so that destroyed cells are immediately ignored
        {
            var type = this.dna[this.cellInstances[i].cellRef].type;

            if(type == TYPES.LOCOMOTOR || type == TYPES.ROTATOR)
            {
                if(this.dna[this.cellInstances[i].cellRef].inputs.length > 0)
                {
                    var sum = sigmoid(this.getCellInputSum(this.cellInstances[i]));

                    //TODO: Act on the sum we've received (ALWAYS between 0 and 1, but VERY rarely exactly 0 or 1)

                    this.cellInstances[i].activate(sum, deltaTime);
                }
            }
        }

        //this.transform.rotation += deltaTime * 60;
        //this.transform.position = this.transform.position.add(this.transform.forward().stretch(deltaTime * 250))
    },

    consumeEnergy: function(energyNeeded) //returns a number between 0 and 1. 0 meaning no energy was consumed (because we ran out), 1 means all of the energy was consumed.
    {
        var stomachRef = -1;
        var amt = 0;

        for(var i = 0; i < this.cellInstances.length; i++)
        {
            if(this.cellInstances[i].type == TYPES.STOMACH)
            {
                if(this.cellInstances[i].energy > amt)
                {
                    stomachRef = i;
                    amt = this.cellInstances[i].energy;
                }
            }
        }

        if(stomachRef == -1)
            return 0;

        var efficiency = Math.min(1, this.cellInstances[stomachRef].energy / energyNeeded)

        this.cellInstances[stomachRef].energy = Math.max(0, this.cellInstances[stomachRef].energy - energyNeeded)

        return efficiency;
    },

    draw: function()
    {
        for(var i = 0; i < this.cellInstances.length; i++)
        {
            if(this.dna[this.cellInstances[i].cellRef].inputs.length > 0)
            {
                for(var c = 0; c < this.dna[this.cellInstances[i].cellRef].inputs.length; c++)
                {
                    var pos = this.findCellInstanceByCellId(this.dna[this.cellInstances[i].cellRef].inputs[c].id).transform.position;

                    GameWindow.canvasContext.strokeStyle = 'red';
                    GameWindow.canvasContext.beginPath();
                    GameWindow.canvasContext.moveTo(this.cellInstances[i].transform.position.x, this.cellInstances[i].transform.position.y);
                    GameWindow.canvasContext.lineTo(pos.x, pos.y);
                    GameWindow.canvasContext.stroke();
                }
            }
        }
    },

    gainEnergy: function(amount)
    {
        var stomachRef = -1;
        var amt = 10000;

        for(var i = 0; i < this.cellInstances.length; i++)
        {
            if(this.cellInstances[i].type == TYPES.STOMACH)
            {
                if(this.cellInstances[i].energy < amt)
                {
                    stomachRef = i;
                    amt = this.cellInstances[i].energy;
                }
            }
        }

        if(stomachRef == -1)
            return 0;

        this.cellInstances[stomachRef].energy = Math.min(cell.maxEnergy, this.cellInstances[stomachRef].energy + amount)
    },

    getCellInputSum: function(cellInstance, tracer = [])
    {
        var cellId = this.dna[cellInstance.cellRef].id;

        if(tracer.indexOf(cellId) >= 0) //is my id already in the tracer?
            return 0;

        tracer.push(cellId)

        var currentSum = cellInstance.currentSensorSignal;
        cellInstance.debugShouldDraw = true;

        if(this.dna[cellInstance.cellRef].inputs.length > 0)
        {
            for(var i = 0; i < this.dna[cellInstance.cellRef].inputs.length; i++)
            {
                var synapse = this.dna[cellInstance.cellRef].inputs[i];

                var cInstance = this.findCellInstanceByCellId(synapse.id);
                var inputSum = this.getCellInputSum(cInstance, tracer);

                currentSum += sigmoid(inputSum * synapse.strength);
            }
        }

        cellInstance.lastSum = currentSum;

        return currentSum + this.dna[cellInstance.cellRef].bias;
    },

    findCellInstanceByCellId: function(id)
    {
        for(var i = 0; i < this.cellInstances.length; i++)
        {
            if(this.dna[this.cellInstances[i].cellRef].id == id)
                return this.cellInstances[i];
        }
    },

    onDestroy: function()
    {
        
    },

    onCollision: function(other)
    {
        
    }
}
 
 
/**
 * DNA strings contain the makeup of a creature, seriving as a template for the creature factory
 * The DNA string must be automatically generatable so that random mutations can occur.
 * 
 * In essence, it is a list of cell types with potential Synapses. Any 'sensor' cell can have one or more connections to 'activatable' cells such as locomotion cells or mouths.
 * 
 * A DNA string should be able to be parsed to a simple string (hence the name), meaning the factory must be able to interpret relations from a data structure that doesn't support references.
 * 
 * definitions:
 * - cell: one out of many objects of uniform size that make up a creature
 * - sensor: a cell that is able to detect things
 * - activator: a cell that can respond to the signal of a Synapse, using energy from Stomach cells. When no energy is available, the Activators are unable to do anything and as such the creature will essentially be 'dead'
 * - Synapse: a connection between a sensor and an activator
 * - braincell: a cell that acts as a hybrid activator/sensor. It receives inputs like an activator would, but instead of doing something on its own, it has outbound Synapse connections of its own to one or more sensors (or other brain cells)
 * 
 * Types of mutations that can occur:
 * - add or remove connections between sensors and activators (unlikely)
 * - add or remove cells of any type (very unlikely)
 * - change the strength of Synapses (likely)
 * 
 * Types of cells:
 * ACTIVATORS
 * - Mouth: can Bite, which will eat whatever is in front of it, be that a cell or a food pellet (TEMP: automatically works without input for now)
 * - Locomotor: When activated, pushes the creature in the direction of the creature's center, relative from the locomotor cell
 * - Rotator: Applies torque when activated. 0.5 is neural, 0 is full left, 1 is full right
 * 
 * SENSORS
 * - Eye: Activates whenever there is a cell or food pellet near it within a certain range and in a certain direction
 * 
 * OTHER
 * - Stomach: Can contain energy. Each cell/pellet eaten increases energy in any available Stomach cells. All Activators require an amount of energy from any of the available Stomach cells to function.
 * - Meat: A cell which does nothing except connect other cells. (and potentially function as armor)
 * - Heart: Does nothing, but when the heart is destroyed the creature will die. Creature cannot have more than one Heart, which is always at 0,0
 * 
 * A creature will multiply when all its Stomach cells are completely full, draining each of them by 50%. This spawns an Egg which will hatch into a copy of the parent creature with a slight mutation applied as soon as there is space available (read: the parent creature has moved suffficiently far away).
 * 
 * Examples:
 * One cell: 
 * {
 *  ID : number. Each new cell will take the first available number so that numbers from deleted cells are re-used
 *  Type: number, corresponding to the cell type.
 *  In: [array of ID's to inbound Synapses and strength, e.g.: {ID:5,Strength:1.2},{ID:8,Strength:1.2}]
 *  Pos: {X, Y} vector, where 0,0 is the center of a creature which will ALWAYS be a Heart. (note that center of mass, as in the average position of all cells, will be used to determine direction for locomotors and such, not 0,0)
 * }
 * 
 * The output of a Cell is determined as follows: 
 * OUT = Sigmoid ( IN1Val*IN1Weight + IN2Val*IN2Weight + IN3Val*IN3Weight + ... )
 * Sigmoid will accept numbers from negative infinity up until positive infinity, and as such the weights of all Synapses need not be clamped. Sigmoid will always output a value between zero and one.
 * 
 * Functionally, outputs will be determined from the Activators. Activators request their inputs to calculate, which in turn request their inputs to calculate, until all values have been determined.
 * 
 * Note that any cells which would be disconnected from the heart should be automatically removed from the DNA string
 * 
 * --
 * 
 * This file will handle:
 * - Generating new DNA strings
 * - Mutating existing DNA strings
 */

/**
 * Potential issues:
 * 
 * Infinite circular connections in braincells.
 * Solutions:
 * - Don't allow braincells to connect to braincells (would fix, but is undesirable as it limits brain complexity)
 * - use value 0 when the last cell of a circle is detected. e.g.: A B and C are a loop. A requests input from B, which requests input from C, which requests input from A. A would complete the circle and start a loop, so instead 0 is returned.
 */

//TODO: Make sort of random maybe
function GenerateNewDNA(startingCells = 10)
{
   var dna = [
      {
         id: 0,
         type: TYPES.HEART,
         inputs: [],
         pos: {x:0, y:0}
      }
   ];

   for(var i = 0; i < startingCells; i++)
      AddRandomCell(dna)

   for(var i = 0; i < startingCells; i++)
      AddRandomSynapse(dna)

   dna[0].inputs = [];

   return dna;
}

function MutateDNA(dna)
{
   var rand = Math.random();

   MutateBiases(dna, 1 - rand);

   if(rand < 0.1)
      AddRandomCell(dna);
   
   if(rand < 0.2)
      AddRandomSynapse(dna);

   if(rand < 0.7)
      MutateSynapses(dna, 1 - rand);
}

function MutateBiases(dna, strength)
{
   for(var i = 0; i < dna.length; i++)
   {
      dna[i].bias += -(strength/2) + (Math.random() * strength);
   }
}

function MutateSynapses(dna, strength)
{
   for(var i = 0; i < dna.length; i++)
   {
      if(dna[i].inputs.length > 0)
      {
         for(var o = 0; o < dna[i].inputs.length; o++)
         {
            dna[i].inputs[o].strength += (-0.5 + Math.random()) * strength;
         }
      }
   }
}

function AddRandomSynapse(dna, att = 0)
{
   if(att > 10)
      return; //can't be done, give up

   var receivers = [];
   var senders = [];

   for(var i = 0; i < dna.length; i++)
   {
      if(dna[i].type == TYPES.BRAIN)
      {
         receivers.push(i);
         senders.push(i);
      }

      if(dna[i].type == TYPES.EYE || dna[i].type == TYPES.STOMACH || dna[i].type == TYPES.NOSE || dna[i].type == TYPES.HEATSENSOR)
      {
         senders.push(i);
      }

      if(dna[i].type == TYPES.LOCOMOTOR || dna[i].type == TYPES.ROTATOR)
      {
         receivers.push(i);
      }
   }

   var ri = Math.floor(Math.random() * receivers.length);
   var si = Math.floor(Math.random() * senders.length)

   var tri = receivers[ri]
   var tsi = senders[si];

   var OK = true

   for(var i = 0; i < dna[tri].inputs.length; i++)
   {
      if(dna[tri].inputs[i].id == dna[tsi].id)
         OK = false; //duplicate
   }

   if(tri != tsi && tri != 0 && tsi != 0 && OK) //todo: check for duplicates
   {
      dna[tri].inputs.push({id:dna[tsi].id, strength: (-0.5 + Math.random())});
   }
   else
   {
      AddRandomSynapse(dna, att + 1); //re-try
   }
}

function AddRandomCell(dna)
{
   var totalWeight = 0;
   var weights = Object.values(Weights);

   for (var i = 0; i < weights.length; i++) 
   {
      totalWeight += weights[i];
   }

   var current = Math.random() * totalWeight;
   var selected;

   for (const [key, value] of Object.entries(Weights)) 
   {
      current -= value;

      if(current <= 0)
      {
         selected = key;
         break;
      }
   }

   var id = 1;

   for(var i = 0; i < dna.length; i++)
   {
      while(id == dna[i].id)
         id++;
   }

   var done = false;
   var pos = {x:0, y:0}

   while(!done)
   {
      done = true;

      for(var i = 0; i < dna.length; i++)
      {
         if(pos.x == dna[i].pos.x && pos.y == dna[i].pos.y)
         {
            done = false;
            var val = Math.random() > 0.5? -1 : 1;
            var key = Math.random() > 0.5? "y" : "x";

            pos[key] += val;
            break;
         }
      }
   }

   var cell = 
   {
      id: id,
      type: TYPES[selected],
      inputs: [],
      pos: pos,
      bias: 0
   }

   dna.push(cell);
}

var TYPES = 
{
   HEART: 0,
   BRAIN: 1,
   MOUTH: 2,
   LOCOMOTOR: 3,
   ROTATOR: 4,
   EYE: 5,
   STOMACH: 6,
   MEAT: 7,
   NOSE: 8,
   HEATSENSOR: 9
}

var Weights = 
{
   BRAIN: 2.2,
   MOUTH: 2,
   LOCOMOTOR: 2,
   ROTATOR: 1,
   EYE: 2.4,
   STOMACH: 3,
   MEAT: 4,
   NOSE: 1.5,
   HEATSENSOR: 1.3
}



function sigmoid(z) 
{
   return (-0.5 + (1 / (1 + Math.exp(-z)))) * 2;
}
 
 
/**
 * Snake Food
 */

var gem =
{
    start: function()
    {
        this.name = "gem";
        this.renderer.subImage = Math.floor(Math.random() * this.renderer.image.subImages);

        this.transform.scale.x = 0;
        this.transform.scale.y = 0;

        var scaleUpTween = new TWEEN.Tween(this.transform.scale);

        scaleUpTween.to({x: [1.5, 1], y: [1.5, 1]}, 800);

        scaleUpTween.easing(TWEEN.Easing.Back.Out);

        scaleUpTween.start();

        this.shakeLength = 1000;
        this.shakes = 4;
        this.shakeAngle = 45;

        this.minShakeTimer = 5;
        this.maxShakeTimer = 20;
        this.shakeTimer = this.minShakeTimer + (Math.random() * (this.maxShakeTimer - this.minShakeTimer));
        this.currentShakeTimer = 0;

        GameAudio.play("bubble");
    },

    update: function(deltaTime)
    {
        this.currentShakeTimer += deltaTime;

        if(this.currentShakeTimer >= this.shakeTimer)
        {
            this.shake();

            this.shakeTimer = this.minShakeTimer + (Math.random() * (this.maxShakeTimer - this.minShakeTimer));

            this.currentShakeTimer = 0;
        }
    },

    onDestroy: function()
    {
        gemSpawner.currentActive--;
    },

    onCollision: function(other)
    {
        if (other.name == "snake" && !this.destroyed) 
        {
            other.grow(1, -1);
            this.destroy();
        }
    },

    shake: function(callback, context)
    {
        var singleShakeTime = this.shakes / this.shakeLength;

        var shakeTween = new TWEEN.Tween(this.transform);

        var angles = [];

        var offset = Math.round(Math.random() * 5);

        for (var i = 0; i < this.shakes; i++)
        {
            var intensiy = 1 - (i / this.shakes);

            // Direction of the shake (is 'i' odd?)
            var dir = !((i + offset) % 2)? -1 : 1;

            var angleRad = this.shakeAngle;

            var angle = angleRad * dir * intensiy;

            angles.push(angle);
        }

        angles.push(0);

        shakeTween.to({rotation: angles}, this.shakeLength);

        shakeTween.easing(TWEEN.Easing.Quadratic.InOut);

        if (callback)
        {
            shakeTween.onComplete.addOnce(callback, context);
        }

        shakeTween.start();
    }
} 
 
/**
 * Gem Spawner spawns gems in the world
 */
var gemSpawner = 
{
    start: function()
    {
        gemSpawner.currentActive = 0;

        this.maxGems = 7;

        for (var i = 0; i < this.maxGems; i++)
        {
            this.spawnGem(i * 200, i == this.maxGems - 1);
        }

        this.spawnInUpdate = false;

        this.allGems = [];
    },

    spawnGem: function(delay, enableSpawningAfter)
    {
        delay = delay || 0;

        if (gemSpawner.currentActive < this.maxGems)
        {
            gemSpawner.currentActive++;

            timingUtil.delay(delay, function()
            {
                var x = 100 + Math.random() * (GameWindow.width - 200);
                var y = 100 + Math.random() * (GameWindow.height - 200);

                this.allGems.push(GameObject.create(gem, x, y, s_gem, 0));

                this.allGems = this.allGems.filter(function(item)
                {
                    return !item.destroyed;
                });

                if (enableSpawningAfter)
                {
                    this.spawnInUpdate = true;
                }
            }, this);
        }
    },

    onDestroy: function()
    {
        for (var i = 0; i < this.allGems.length; i++)
        {
            this.allGems[i].destroy();
        }
    },

    update: function(deltaTime)
    {
        if (this.spawnInUpdate)
        {
            this.spawnGem(500 + (Math.random() * 500));
        }
    }
} 
 
/***
 * All images used in the game are  initialized here
 */

var s_head = new image("images/head.png", 4, 4);
var s_orb = new image("images/orb.png", 8, 4);
var s_cell = new image("images/cells.png", 12, 4);
var s_gem = new image("images/gems.png", 72, 6); 
 
/**
 * Main script
 */

window.onload = init;

function DoInheritance()
{
    //resultclass = GameObject.inherit(baseclass, childclass);
}

function init()
{
    DoInheritance();
    //Game.initialize(xpos, ypos, width, height, fps, callback)
    // -2 to account for borders
    var width = window.innerWidth - 2;
    var height = window.innerHeight - 2;

    Game.initialize(0, 0, width, height, gameStart);
}

function gameStart()
{
	GameWindow.backgroundColor = {r:100, g:149, b:237};

	Game.setUpdateFunction(update); // Let the game engine know we want to link our 'update()' to the game's update step.

	GameAudio.init(sounds, soundsFolder);

    p1 = GameObject.create(gemSpawner, GameWindow.width / 2, GameWindow.height / 2, null, 500);
    tester = GameObject.create(creature, GameWindow.width / 3, GameWindow.height / 2, null, 500);

    tester.dna = GenerateNewDNA();
    tester.initCells();

    tester2 = GameObject.create(creature, (GameWindow.width / 3) * 2, GameWindow.height / 2, null, 500);

    tester2.dna = GenerateNewDNA();
    tester2.initCells();
}

function update(deltaTime)
{  
   
}

 
 
/**
 * All sounds in the game
 */

var soundsFolder = "sounds";
var sounds = 
[
	{
        name: "bubble",
        volume: 1
    },
    {
        name: "glass",
        volume: 0.9
    },
    {
        name: "tap",
        volume: 1
    },
    {
        name: "bite",
        volume: 1
    },
    {
        name: "pop",
        volume: 0.1
    }

]; //List of all sound files as strings (do not include file extension) 
 
