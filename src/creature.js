/**
 * It's a thing!
 */

var creature =
{
    dna: {},
    cellInstances: [],

    start: function()
    {

    },

    initCells()
    {
        for(var i = 0; i < this.dna.length; i++)
        {
            var newCell = GameObject.create(cell, this.transform.position.x + (this.dna[i].pos.x * 32), this.transform.position.y + (this.dna[i].pos.y * 32), s_cell, 400);

            newCell.setType(this.dna[i].type);
            newCell.parentCreature = this;
            newCell.localPos = new vector(this.dna[i].pos.x * 32, this.dna[i].pos.y * 32)

            newCell.cellRef = i;

            this.cellInstances.push(newCell);
        }
    },

    update: function(deltaTime)
    {
        for(var i = this.cellInstances.length -1; i >= 0; i--) //reverse loop for deletion of references to destroyed cells
        {
            if(this.cellInstances[i].destroyed)
                this.cellInstances.splice(i, 1);
        }

        for(var i = 0; i < this.cellInstances.length; i++) //loop through instances instead of dna so that destroyed cells are immediately ignored
        {
            var type = this.dna[this.cellInstances[i].cellRef].type;

            if(type == TYPES.LOCOMOTOR || type == TYPES.ROTATOR)
            {
                if(this.dna[this.cellInstances[i].cellRef].inputs.length > 0)
                {
                    var sum = sigmoid(this.getCellInputSum(this.cellInstances[i]));

                    //TODO: Act on the sum we've received (ALWAYS between 0 and 1, but VERY rarely exactly 0 or 1)

                    this.cellInstances[i].activate(sum, deltaTime);
                }
            }
        }

        //this.transform.rotation += deltaTime * 60;
        //this.transform.position = this.transform.position.add(this.transform.forward().stretch(deltaTime * 250))
    },

    consumeEnergy: function(energyNeeded) //returns a number between 0 and 1. 0 meaning no energy was consumed (because we ran out), 1 means all of the energy was consumed.
    {
        var stomachRef = -1;
        var amt = 0;

        for(var i = 0; i < this.cellInstances.length; i++)
        {
            if(this.cellInstances[i].type == TYPES.STOMACH)
            {
                if(this.cellInstances[i].energy > amt)
                {
                    stomachRef = i;
                    amt = this.cellInstances[i].energy;
                }
            }
        }

        if(stomachRef == -1)
            return 0;

        var efficiency = Math.min(1, this.cellInstances[stomachRef].energy / energyNeeded)

        this.cellInstances[stomachRef].energy = Math.max(0, this.cellInstances[stomachRef].energy - energyNeeded)

        return efficiency;
    },

    draw: function()
    {
        for(var i = 0; i < this.cellInstances.length; i++)
        {
            if(this.dna[this.cellInstances[i].cellRef].inputs.length > 0)
            {
                for(var c = 0; c < this.dna[this.cellInstances[i].cellRef].inputs.length; c++)
                {
                    var pos = this.findCellInstanceByCellId(this.dna[this.cellInstances[i].cellRef].inputs[c].id).transform.position;

                    GameWindow.canvasContext.strokeStyle = 'red';
                    GameWindow.canvasContext.beginPath();
                    GameWindow.canvasContext.moveTo(this.cellInstances[i].transform.position.x, this.cellInstances[i].transform.position.y);
                    GameWindow.canvasContext.lineTo(pos.x, pos.y);
                    GameWindow.canvasContext.stroke();
                }
            }
        }
    },

    gainEnergy: function(amount)
    {
        var stomachRef = -1;
        var amt = 10000;

        for(var i = 0; i < this.cellInstances.length; i++)
        {
            if(this.cellInstances[i].type == TYPES.STOMACH)
            {
                if(this.cellInstances[i].energy < amt)
                {
                    stomachRef = i;
                    amt = this.cellInstances[i].energy;
                }
            }
        }

        if(stomachRef == -1)
            return 0;

        this.cellInstances[stomachRef].energy = Math.min(cell.maxEnergy, this.cellInstances[stomachRef].energy + amount)
    },

    getCellInputSum: function(cellInstance, tracer = [])
    {
        var cellId = this.dna[cellInstance.cellRef].id;

        if(tracer.indexOf(cellId) >= 0) //is my id already in the tracer?
            return 0;

        tracer.push(cellId)

        var currentSum = cellInstance.currentSensorSignal;
        cellInstance.debugShouldDraw = true;

        if(this.dna[cellInstance.cellRef].inputs.length > 0)
        {
            for(var i = 0; i < this.dna[cellInstance.cellRef].inputs.length; i++)
            {
                var synapse = this.dna[cellInstance.cellRef].inputs[i];

                var cInstance = this.findCellInstanceByCellId(synapse.id);
                var inputSum = this.getCellInputSum(cInstance, tracer);

                currentSum += sigmoid(inputSum * synapse.strength);
            }
        }

        cellInstance.lastSum = currentSum;

        return currentSum + this.dna[cellInstance.cellRef].bias;
    },

    findCellInstanceByCellId: function(id)
    {
        for(var i = 0; i < this.cellInstances.length; i++)
        {
            if(this.dna[this.cellInstances[i].cellRef].id == id)
                return this.cellInstances[i];
        }
    },

    onDestroy: function()
    {
        
    },

    onCollision: function(other)
    {
        
    }
}
