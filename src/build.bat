@echo off

del .\js\game.js

for %%I in (.\src\*.js) do (
    echo %%~fI
    type %%I >> .\js\game.js
    echo. >> .\js\game.js
    echo. >> .\js\game.js
)

For /f "tokens=2-4 delims=/ " %%a in ('date /t') do (set mydate=%%c-%%a-%%b)
For /f "tokens=1-2 delims=/:" %%a in ("%TIME%") do (set mytime=%%a:%%b)
echo %mydate% %mytime% >> .\js\lastbuild.txt