/***
 * All images used in the game are  initialized here
 */

var s_head = new image("images/head.png", 4, 4);
var s_orb = new image("images/orb.png", 8, 4);
var s_cell = new image("images/cells.png", 12, 4);
var s_gem = new image("images/gems.png", 72, 6);