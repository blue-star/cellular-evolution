/**
 * Main script
 */

window.onload = init;

function DoInheritance()
{
    //resultclass = GameObject.inherit(baseclass, childclass);
}

function init()
{
    DoInheritance();
    //Game.initialize(xpos, ypos, width, height, fps, callback)
    // -2 to account for borders
    var width = window.innerWidth - 2;
    var height = window.innerHeight - 2;

    Game.initialize(0, 0, width, height, gameStart);
}

function gameStart()
{
	GameWindow.backgroundColor = {r:100, g:149, b:237};

	Game.setUpdateFunction(update); // Let the game engine know we want to link our 'update()' to the game's update step.

	GameAudio.init(sounds, soundsFolder);

    p1 = GameObject.create(gemSpawner, GameWindow.width / 2, GameWindow.height / 2, null, 500);
    tester = GameObject.create(creature, GameWindow.width / 3, GameWindow.height / 2, null, 500);

    tester.dna = GenerateNewDNA();
    tester.initCells();

    tester2 = GameObject.create(creature, (GameWindow.width / 3) * 2, GameWindow.height / 2, null, 500);

    tester2.dna = GenerateNewDNA();
    tester2.initCells();
}

function update(deltaTime)
{  
   
}

