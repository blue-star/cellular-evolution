/**
 * DNA strings contain the makeup of a creature, seriving as a template for the creature factory
 * The DNA string must be automatically generatable so that random mutations can occur.
 * 
 * In essence, it is a list of cell types with potential Synapses. Any 'sensor' cell can have one or more connections to 'activatable' cells such as locomotion cells or mouths.
 * 
 * A DNA string should be able to be parsed to a simple string (hence the name), meaning the factory must be able to interpret relations from a data structure that doesn't support references.
 * 
 * definitions:
 * - cell: one out of many objects of uniform size that make up a creature
 * - sensor: a cell that is able to detect things
 * - activator: a cell that can respond to the signal of a Synapse, using energy from Stomach cells. When no energy is available, the Activators are unable to do anything and as such the creature will essentially be 'dead'
 * - Synapse: a connection between a sensor and an activator
 * - braincell: a cell that acts as a hybrid activator/sensor. It receives inputs like an activator would, but instead of doing something on its own, it has outbound Synapse connections of its own to one or more sensors (or other brain cells)
 * 
 * Types of mutations that can occur:
 * - add or remove connections between sensors and activators (unlikely)
 * - add or remove cells of any type (very unlikely)
 * - change the strength of Synapses (likely)
 * 
 * Types of cells:
 * ACTIVATORS
 * - Mouth: can Bite, which will eat whatever is in front of it, be that a cell or a food pellet (TEMP: automatically works without input for now)
 * - Locomotor: When activated, pushes the creature in the direction of the creature's center, relative from the locomotor cell
 * - Rotator: Applies torque when activated. 0.5 is neural, 0 is full left, 1 is full right
 * 
 * SENSORS
 * - Eye: Activates whenever there is a cell or food pellet near it within a certain range and in a certain direction
 * 
 * OTHER
 * - Stomach: Can contain energy. Each cell/pellet eaten increases energy in any available Stomach cells. All Activators require an amount of energy from any of the available Stomach cells to function.
 * - Meat: A cell which does nothing except connect other cells. (and potentially function as armor)
 * - Heart: Does nothing, but when the heart is destroyed the creature will die. Creature cannot have more than one Heart, which is always at 0,0
 * 
 * A creature will multiply when all its Stomach cells are completely full, draining each of them by 50%. This spawns an Egg which will hatch into a copy of the parent creature with a slight mutation applied as soon as there is space available (read: the parent creature has moved suffficiently far away).
 * 
 * Examples:
 * One cell: 
 * {
 *  ID : number. Each new cell will take the first available number so that numbers from deleted cells are re-used
 *  Type: number, corresponding to the cell type.
 *  In: [array of ID's to inbound Synapses and strength, e.g.: {ID:5,Strength:1.2},{ID:8,Strength:1.2}]
 *  Pos: {X, Y} vector, where 0,0 is the center of a creature which will ALWAYS be a Heart. (note that center of mass, as in the average position of all cells, will be used to determine direction for locomotors and such, not 0,0)
 * }
 * 
 * The output of a Cell is determined as follows: 
 * OUT = Sigmoid ( IN1Val*IN1Weight + IN2Val*IN2Weight + IN3Val*IN3Weight + ... )
 * Sigmoid will accept numbers from negative infinity up until positive infinity, and as such the weights of all Synapses need not be clamped. Sigmoid will always output a value between zero and one.
 * 
 * Functionally, outputs will be determined from the Activators. Activators request their inputs to calculate, which in turn request their inputs to calculate, until all values have been determined.
 * 
 * Note that any cells which would be disconnected from the heart should be automatically removed from the DNA string
 * 
 * --
 * 
 * This file will handle:
 * - Generating new DNA strings
 * - Mutating existing DNA strings
 */

/**
 * Potential issues:
 * 
 * Infinite circular connections in braincells.
 * Solutions:
 * - Don't allow braincells to connect to braincells (would fix, but is undesirable as it limits brain complexity)
 * - use value 0 when the last cell of a circle is detected. e.g.: A B and C are a loop. A requests input from B, which requests input from C, which requests input from A. A would complete the circle and start a loop, so instead 0 is returned.
 */

//TODO: Make sort of random maybe
function GenerateNewDNA(startingCells = 10)
{
   var dna = [
      {
         id: 0,
         type: TYPES.HEART,
         inputs: [],
         pos: {x:0, y:0}
      }
   ];

   for(var i = 0; i < startingCells; i++)
      AddRandomCell(dna)

   for(var i = 0; i < startingCells; i++)
      AddRandomSynapse(dna)

   dna[0].inputs = [];

   return dna;
}

function MutateDNA(dna)
{
   var rand = Math.random();

   MutateBiases(dna, 1 - rand);

   if(rand < 0.1)
      AddRandomCell(dna);
   
   if(rand < 0.2)
      AddRandomSynapse(dna);

   if(rand < 0.7)
      MutateSynapses(dna, 1 - rand);
}

function MutateBiases(dna, strength)
{
   for(var i = 0; i < dna.length; i++)
   {
      dna[i].bias += -(strength/2) + (Math.random() * strength);
   }
}

function MutateSynapses(dna, strength)
{
   for(var i = 0; i < dna.length; i++)
   {
      if(dna[i].inputs.length > 0)
      {
         for(var o = 0; o < dna[i].inputs.length; o++)
         {
            dna[i].inputs[o].strength += (-0.5 + Math.random()) * strength;
         }
      }
   }
}

function AddRandomSynapse(dna, att = 0)
{
   if(att > 10)
      return; //can't be done, give up

   var receivers = [];
   var senders = [];

   for(var i = 0; i < dna.length; i++)
   {
      if(dna[i].type == TYPES.BRAIN)
      {
         receivers.push(i);
         senders.push(i);
      }

      if(dna[i].type == TYPES.EYE || dna[i].type == TYPES.STOMACH || dna[i].type == TYPES.NOSE || dna[i].type == TYPES.HEATSENSOR)
      {
         senders.push(i);
      }

      if(dna[i].type == TYPES.LOCOMOTOR || dna[i].type == TYPES.ROTATOR)
      {
         receivers.push(i);
      }
   }

   var ri = Math.floor(Math.random() * receivers.length);
   var si = Math.floor(Math.random() * senders.length)

   var tri = receivers[ri]
   var tsi = senders[si];

   var OK = true

   for(var i = 0; i < dna[tri].inputs.length; i++)
   {
      if(dna[tri].inputs[i].id == dna[tsi].id)
         OK = false; //duplicate
   }

   if(tri != tsi && tri != 0 && tsi != 0 && OK) //todo: check for duplicates
   {
      dna[tri].inputs.push({id:dna[tsi].id, strength: (-0.5 + Math.random())});
   }
   else
   {
      AddRandomSynapse(dna, att + 1); //re-try
   }
}

function AddRandomCell(dna)
{
   var totalWeight = 0;
   var weights = Object.values(Weights);

   for (var i = 0; i < weights.length; i++) 
   {
      totalWeight += weights[i];
   }

   var current = Math.random() * totalWeight;
   var selected;

   for (const [key, value] of Object.entries(Weights)) 
   {
      current -= value;

      if(current <= 0)
      {
         selected = key;
         break;
      }
   }

   var id = 1;

   for(var i = 0; i < dna.length; i++)
   {
      while(id == dna[i].id)
         id++;
   }

   var done = false;
   var pos = {x:0, y:0}

   while(!done)
   {
      done = true;

      for(var i = 0; i < dna.length; i++)
      {
         if(pos.x == dna[i].pos.x && pos.y == dna[i].pos.y)
         {
            done = false;
            var val = Math.random() > 0.5? -1 : 1;
            var key = Math.random() > 0.5? "y" : "x";

            pos[key] += val;
            break;
         }
      }
   }

   var cell = 
   {
      id: id,
      type: TYPES[selected],
      inputs: [],
      pos: pos,
      bias: 0
   }

   dna.push(cell);
}

var TYPES = 
{
   HEART: 0,
   BRAIN: 1,
   MOUTH: 2,
   LOCOMOTOR: 3,
   ROTATOR: 4,
   EYE: 5,
   STOMACH: 6,
   MEAT: 7,
   NOSE: 8,
   HEATSENSOR: 9
}

var Weights = 
{
   BRAIN: 2.2,
   MOUTH: 2,
   LOCOMOTOR: 2,
   ROTATOR: 1,
   EYE: 2.4,
   STOMACH: 3,
   MEAT: 4,
   NOSE: 1.5,
   HEATSENSOR: 1.3
}



function sigmoid(z) 
{
   return (-0.5 + (1 / (1 + Math.exp(-z)))) * 2;
}
