/**
 * Gem Spawner spawns gems in the world
 */
var gemSpawner = 
{
    start: function()
    {
        gemSpawner.currentActive = 0;

        this.maxGems = 7;

        for (var i = 0; i < this.maxGems; i++)
        {
            this.spawnGem(i * 200, i == this.maxGems - 1);
        }

        this.spawnInUpdate = false;

        this.allGems = [];
    },

    spawnGem: function(delay, enableSpawningAfter)
    {
        delay = delay || 0;

        if (gemSpawner.currentActive < this.maxGems)
        {
            gemSpawner.currentActive++;

            timingUtil.delay(delay, function()
            {
                var x = 100 + Math.random() * (GameWindow.width - 200);
                var y = 100 + Math.random() * (GameWindow.height - 200);

                this.allGems.push(GameObject.create(gem, x, y, s_gem, 0));

                this.allGems = this.allGems.filter(function(item)
                {
                    return !item.destroyed;
                });

                if (enableSpawningAfter)
                {
                    this.spawnInUpdate = true;
                }
            }, this);
        }
    },

    onDestroy: function()
    {
        for (var i = 0; i < this.allGems.length; i++)
        {
            this.allGems[i].destroy();
        }
    },

    update: function(deltaTime)
    {
        if (this.spawnInUpdate)
        {
            this.spawnGem(500 + (Math.random() * 500));
        }
    }
}