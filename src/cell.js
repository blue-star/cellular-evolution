/**
 * Part of a creature!
 */

var cell = 
{
    localPos: new vector(0, 0),
    parentCreature: null,
    type: -1,
    cellRef: -1,
    currentSensorSignal: 0, //only used for sensors, else forever zero
    lastSum: 0, //used for debug purposes
    energy: 1,
    maxEnergy: 1,
    debugShouldDraw: false,

    start: function()
    {
        this.transform.scale.x = 0;
        this.transform.scale.y = 0;

        var scaleUpTween = new TWEEN.Tween(this.transform.scale);

        scaleUpTween.to({x: [1.5, 1], y: [1.5, 1]}, Math.random() * 3200);

        scaleUpTween.easing(TWEEN.Easing.Back.Out);

        scaleUpTween.start();
    },

    activate: function(strength, deltaTime)
    {
        this.lastSum = strength;

        switch(this.type)
        {
            case TYPES.LOCOMOTOR:
                var actualStrength = Math.max(0, strength)
                if(actualStrength > 0)
                {
                    var power = this.parentCreature.consumeEnergy(actualStrength * deltaTime)
                    var rotated = this.localPos.rotate(this.parentCreature.transform.rotation);

                    this.parentCreature.transform.position = this.parentCreature.transform.position.subtract(rotated.stretch(deltaTime * power));
                }
                break;
            case TYPES.ROTATOR:
                var power = this.parentCreature.consumeEnergy(Math.abs(strength) * deltaTime)
                this.parentCreature.transform.rotation += (strength) * deltaTime * 100 * power;
                break;
        }
    },

    onCollision: function(other)
    {
        if (this.type == TYPES.MOUTH && !this.destroyed) 
        {
            if(other.parentCreature == this.parentCreature)
                return;

            this.parentCreature.gainEnergy(this.maxEnergy / 3);
            other.destroy();
        }
    },

    draw: function()
    {
        this.defaultDraw();

        if(this.debugShouldDraw)
        {
            GameWindow.canvasContext.fillStyle = 'black';
            GameWindow.canvasContext.fillText(this.lastSum, this.transform.position.x, this.transform.position.y);
        }
    },

    update: function()
    {
        var rotated = this.localPos.rotate(this.parentCreature.transform.rotation);
        this.transform.position = rotated.stretch(this.transform.scale.x).add(this.parentCreature.transform.position);

        if(this.type == TYPES.EYE)
            this.transform.rotation = rotated.toAngle();
        
        if(this.type == TYPES.LOCOMOTOR)
            this.transform.rotation = rotated.toAngle() - 90;

        if(this.type == TYPES.STOMACH)
            this.currentSensorSignal = (this.energy / this.maxEnergy);
    },

    setType: function(typeID)
    {
        this.renderer.subImage = typeID;
        this.type = typeID;
    }
}